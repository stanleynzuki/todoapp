

# IMPORTTANT DOCKER COMMANDS

## docker build -t my-docker:tag . => Build docker image

## docker run -p 8080:3000 my-docker:tag => run docker image

## docker ps => view containers

## docker stop $(docker ps -q)  => stop all containers

## docker stop <CONTAINER ID> stop one containers

## docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <CONTAINER NAME> => get container import

## docker pull postgres => get postgres Image


## docker run --name <postgres container name> -e POSTGRES_USER=<postgres user> -e POSTGRES_PASSWORD=<postgres password> -e POSTGRES_DB=<database name> -p 5433:5432 -d <db user> => create postgres database 

## docker exec -it <CONTAINER NAME> psql -U <DB USER> <DB NAME> => open psql shell




