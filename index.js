var express = require('express');
var multer = require('multer')
var upload = multer();
const session = require('express-session');
const cookieParser = require('cookie-parser');

var app = express()
app.use(upload.array())

app.use(cookieParser());
app.use(session({secret:'Shh, it is a secret!', resave:true,saveUninitialized:true}));


const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

var routes = require("./routes.js")
app.use("/", routes)

app.listen(3000);