var express = require('express')
var router = express.Router();
var { Pool } = require('pg');
var jwt = require('jsonwebtoken')
var bcrypt = require('bcrypt');


require('dotenv').config();


//Connect to database
const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT
})

//register user
router.post("/register", async function (req, res) {
    const query = "INSERT INTO users (full_name,email,password,created_at) VALUES($1,$2,$3,$4)";
    await pool.query(query, [
        req.body.full_name,
        req.body.email,
        bcrypt.hashSync(req.body.password, 10),
        new Date()
    ])
    res.json({ message: "User added successfully!!" })
})

//verify token
async function verifyToken(req, res) {
    try {
        jwt.verify(req.session.user.token, 'secret_key');
        return true;
    } catch (e) {
        return false;
    }
}


//login
router.post("/login", async function (req, res) {
    var password = req.body.password;
    var email = req.body.email;

    const query = "SELECT * FROM users where email = $1 limit 1";
    const result = await pool.query(query, [email]);
    const user = result.rows[0];
    if (result.rows.length > 0) {
        if (bcrypt.compareSync(password, user.password)) {
            const token = jwt.sign({ user_id: user.user_id, full_name: user.full_name, email: email }, 'secret_key');
            if (req.session.user) {
                req.session.user = {};
                req.session.user = {
                    user_id: user.user_id, full_name: user.full_name, email: email, token: token
                }
            } else {
                req.session.user = {
                    user_id: user.user_id, full_name: user.full_name, email: email, token: token
                }
            }

            res.json({ message: "Login successfull" })
        } else {
            res.json({ message: "The password is incorrect!!" })
        }
    } else {
        res.json({ message: "Incorrect login credentials" })
    }
})

//logout
router.post("/logout", async function(req,res){
    req.session.destroy();
    res.json("Logout successfull!!")
})


//Get all tasks
router.get("/tasks", async function (req, res) {
    if (await verifyToken(req, res)) {
        const query = "SELECT * from tasks";
        const result = await pool.query(query);
        res.json(result.rows)
    } else {
        res.json("You are not logged in!")
    }

})

//Get all completed tasks
router.get("/completed_tasks", async function (req, res) {
    if (await verifyToken(req, res)) {
        const query = "SELECT * from tasks where status = 'Completed'";
        const result = await pool.query(query);
        res.json(result.rows)
    } else {
        res.json("You are not logged in!")
    }
})

//Get all pending tasks
router.get("/pending_tasks", async function (req, res) {
    if (await verifyToken(req, res)) {
        const query = "SELECT * from tasks where status = 'Pending'";
        const result = await pool.query(query);
        res.json(result.rows)
    } else {
        res.json("You are not logged in!")
    }
})

//Get one task by id
router.get("/tasks/:id", async function (req, res) {
    if (await verifyToken(req, res)) {
        const query = "SELECT * FROM tasks where task_id = $1";
        const result = await pool.query(query, [req.params.id])
        res.json(result.rows)
    } else {
        res.json("You are not logged in!")
    }
})

//Add task
router.post("/add_task", async function (req, res) {
    if (await verifyToken(req, res)) {
        const task_name = req.body.task_name;
        const created_at = new Date();
        const query = "INSERT INTO tasks (task_name,created_at) VALUES ($1,$2)";
        await pool.query(query, [task_name, created_at])
        res.json({ message: "Task added successfully!" })
    } else {
        res.json("You are not logged in!")
    }

})

//Update task info
router.put("/update_task_info/:id", async function (req, res) {
    if (await verifyToken(req, res)) {
        const task_name = req.body.task_name;
        const updated_at = new Date();
        const query = "UPDATE tasks set task_name = $1,updated_at=$2 WHERE task_id=$3";
        const result = await pool.query(query, [task_name, updated_at, req.params.id])
        if (result.rowCount > 0) {
            res.json({ message: "Task info updated successfully!" });
        } else {
            res.json({ message: "Update task info error: Task does not exist" });
        }
    } else {
        res.json("You are not logged in!")
    }
});

//Update task status
router.put("/update_task_status/:id", async function (req, res) {
    if (await verifyToken(req, res)) {
        const status = req.body.status;
        const updated_at = new Date();
        const query = "UPDATE tasks set status = $1, updated_at = $2 where task_id = $3";
        const result = await pool.query(query, [status, updated_at, req.params.id]);
        if (result.rowCount > 0) {
            res.json({ message: "Task status updated successfully!" });
        } else {
            res.json({ message: "Update status error: Task does not exist" });
        }
    } else {
        res.json("You are not logged in!")
    }
});

//Delete task
router.delete("/delete_task/:id", async function (req, res) {
    if (await verifyToken(req, res)) {
        const query = "DELETE from tasks where task_id = $1";
        const result = await pool.query(query, [req.params.id]);
        if (result.rowCount > 0) {
            res.json({ message: "Task deleted successfully" });
        } else {
            res.json({ message: "Delete error:Task does not exist" });
        }
    } else {
        res.json("You are not logged in!")
    }
})

module.exports = router;
